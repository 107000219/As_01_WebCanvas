const canvas = document.querySelector('#canvas');
const ctx = canvas.getContext('2d');



ctx.lineJoin = 'round';  // 兩條線交匯處產生 "圓形" 邊角
ctx.lineCap = 'round';  // 筆觸預設為 "圓形"
let isDrawing = false;  // 是否允許繪製  (或說是否是 mousedown 下筆狀態)
let lWidth = 13;//筆刷大小
let tsize = 1000;//文字大小
let lastX = 0;
let lastY = 0;
let lcolor = 'Black';
let isCircle = false;
let isTriangle = false;
let isRectangle = false;
let step = -1;
let history = [];
var nn;
var picture;
var start;
Push();
function test(n){

  if(n==1){
    nn=1;
    document.body.style.cursor = "url('pen.png'), auto";
    pen();
  }
  else if(n==2){
    document.body.style.cursor ="default";
    console.log("6666");
    nn=2;
    barrel();
  }
  else if(n==3){
    nn=3;
    document.body.style.cursor = "url('eraser.png'), auto";
    eraser();
  }
  else if(n==5){
    document.body.style.cursor = "se-resize";
    isCircle = false;
    isTriangle = true;
    isRectangle = false;
    trc();
  }
  else if(n==6){
    document.body.style.cursor ="se-resize";
    isCircle = false;
    isTriangle = false;
    isRectangle = true;
    trc();
  }
  else if(n=7){
    document.body.style.cursor ="se-resize";
    isCircle = true;
    isTriangle = false;
    isRectangle = false;
    trc();
  }

}


function pen(e) {

    console.log("pen");
    if(nn==1){  
      Push();
    ctx.lineWidth = lWidth;  // 筆頭寬度
    ctx.strokeStyle = lcolor ;  // 筆觸顏色
    canvas.addEventListener('mouseup', () => isDrawing = false, );
    canvas.addEventListener('mouseout', () => isDrawing = false, );
    canvas.addEventListener('mousedown', (e) => {
    isDrawing = true;
  [lastX, lastY] = [e.offsetX, e.offsetY]; // 設定起始點
  });

    canvas.addEventListener('mousemove', pen);
    if(!isDrawing) return; 
    ctx.beginPath();  // 開始路徑 or Reset
    ctx.moveTo(lastX, lastY);  // 設定起點
    ctx.lineTo(e.offsetX, e.offsetY);  // 設定終點
    
    console.log("pback");
    ctx.stroke();// 依照設定開始繪製
    [lastX, lastY] = [e.offsetX, e.offsetY];

  }
}


function eraser(e){

  if(nn==3){
    Push();
  ctx.lineWidth = lWidth;  // 筆頭寬度
  canvas.addEventListener('mouseup', () => isDrawing = false, );
  canvas.addEventListener('mouseout', () => isDrawing = false, );
  canvas.addEventListener('mousedown', (e) => {
  isDrawing = true;
  [lastX, lastY] = [e.offsetX, e.offsetY]; // 設定起始點
  });
    canvas.addEventListener('mousemove', eraser);
    let lastX = 0;
    let lastY = 0;
    if(!isDrawing) 
      return;
    ctx.clearRect(e.offsetX, e.offsetY, lWidth, lWidth);
}
    [lastX, lastY] = [e.offsetX, e.offsetY];
    
  }


function reset(){
  Push();
  document.body.style.cursor ="default";
  ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function barrel(e){
  if(nn==2){
    Push();
    console.log("整個螢幕上色");
    ctx.fillStyle = lcolor;  // 筆觸顏色
    ctx.fillRect(0,0,canvas.width,canvas.height);


  }
}

function save() {
    document.body.style.cursor ="default";
    let imgUrl = canvas.toDataURL("image/png");
    let saveA = document.createElement("a");
    document.body.appendChild(saveA);
    saveA.href = imgUrl;
    saveA.download = "zspic" + (new Date).getTime();
    saveA.target = "_blank";
    saveA.click();
};



var m;
function texton(){
  document.body.style.cursor ="default";
  fontS = document.getElementById("abc");
  m=fontS.value;
  if(m==1){
    Push();
    var textt = document.getElementById("textt").value;
    ctx.fillStyle = lcolor;
    var twidth = lWidth;
    ctx.font = twidth+ "px" + " Courier";
    ctx.fillText(textt, 100, 300);
    console.log("courier");
  }
  else if(m==2){
    Push();
    var textt = document.getElementById("textt").value;
    ctx.fillStyle = lcolor;
    var twidth = lWidth;
    ctx.font = twidth+ "px" + " Arial";
    ctx.fillText(textt, 100, 300);
    console.log("ariel");
  }
  else if(m==3){
    Push();
    var textt = document.getElementById("textt").value;
    ctx.fillStyle = lcolor;
    var twidth = lWidth;
    ctx.font = twidth+ "px" + " Comic Sans";
    ctx.fillText(textt, 100, 300);
    console.log("comic");
  }
}


range.onchange = function(){
  lWidth = this.value;
};

textt.onchange = function(){
  tsize = this.value;
  console.log("change");
} 

color.onchange = function(){
  lcolor = this.value;
  console.log("-");
}

var imageLoader = document.getElementById('imageLoader');
    imageLoader.addEventListener('change', handleImage, false);
function handleImage(e){
  Push();
  document.body.style.cursor ="default";
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img,0,0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);     
}



     
function Push() {
    step++;
    if (step < history.length - 1) {
    history.length = step + 1
    }
    history.push(canvas.toDataURL()); 
}                  

function undo() {
    if (step > 0) {
    step-1;
    let canvaspic = new Image(); 
    canvaspic.src = history[step]; 
    canvaspic.onload = function() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(canvaspic, 0, 0) 
    }
}           
}         
function redo() {
    if (step < history.length-1) {
        step+1;
        let canvasPic = new Image();
        canvasPic.src = history[step];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}

